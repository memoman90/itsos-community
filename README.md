# ITSOS
## a community of learners scrapping together for more and more technical knowledge and training.

**ITSOS** is a young collection of people anxious to shape a world where we have skilled IT professionals that can fill any needed position in the market with ease and efficiency.

**Developed using Hugo framework**

[![screenshot](https://raw.githubusercontent.com/gohugoio/hugoDocs/master/static/img/hugo-logo.png)](https://gohugo.io/)

--------------------------------------------------------------------------------------------------------
Founder :

Mohammed Fouad Hassen A.K.A memo ;)
